#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jstring JNICALL Java_library_eliminate_1
  (JNIEnv *env, jobject object, jstring searched, jstring inString)
{
    javaEnv = env;
    const char* c_searched = toString(searched);
    const char* c_inString = toString(inString);
    const char* c_outValue = eliminate(c_searched, c_inString);
    return toJstring(c_outValue);
}

JNIEXPORT jboolean JNICALL Java_library_equals_1
  (JNIEnv *env, jobject object, jstring leftString, jstring rightString)
{
    javaEnv = env;
    const char* c_leftString = toString(leftString);
    const char* c_rightString = toString(rightString);
    bool c_outValue = equals(c_leftString, c_rightString);
    return toJboolean(c_outValue);
}

JNIEXPORT jint JNICALL Java_library_frequency_1
  (JNIEnv *env, jobject object, jchar character, jstring inString)
{
    javaEnv = env;
    char c_character = toChar(character);
    const char* c_inString = toString(inString);
    int c_outValue = frequency(c_character, c_inString);
    return toJint(c_outValue);
}

JNIEXPORT jint JNICALL Java_library_length_1
  (JNIEnv *env, jobject object, jstring inString)
{
    javaEnv = env;
    const char* c_inString = toString(inString);
    int c_outValue = length(c_inString);
    return toJint(c_outValue);
}

JNIEXPORT jint JNICALL Java_library_offset_1
  (JNIEnv *env, jobject object, jchar character, jstring inString)
{
    javaEnv = env;
    char c_character = toChar(character);
    const char* c_inString = toString(inString);
    int c_outValue = offset(c_character, c_inString);
    return toJint(c_outValue);
}

JNIEXPORT jstring JNICALL Java_library_piece_1
  (JNIEnv *env, jobject object, jint from, jint to, jstring inString)
{
    javaEnv = env;
    int c_from = toInt(from);
    int c_to = toInt(to);
    const char* c_inString = toString(inString);
    const char* c_outValue = piece(c_from, c_to, c_inString);
    return toJstring(c_outValue);
}

JNIEXPORT jstring JNICALL Java_library_replace_1
  (JNIEnv *env, jobject object, jstring searched, jstring replacement, jstring inString)
{
    javaEnv = env;
    const char* c_searched = toString(searched);
    const char* c_replacement = toString(replacement);
    const char* c_inString = toString(inString);
    const char* c_outValue = replace(c_searched, c_replacement, c_inString);
    return toJstring(c_outValue);
}

JNIEXPORT jstring JNICALL Java_library_toLowerCase_1
  (JNIEnv *env, jobject object, jstring inString)
{
    javaEnv = env;
    const char* c_inString = toString(inString);
    const char* c_outValue = toLowerCase(c_inString);
    return toJstring(c_outValue);
}

