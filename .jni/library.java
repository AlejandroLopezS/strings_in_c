/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "eliminate(const char*,const char*):const char*")
    public String eliminate(String searched, String inString){
        return eliminate_(searched, inString);
    }
    private native String eliminate_(String searched, String inString);

    @executerpane.MethodAnnotation(signature = "equals(const char*,const char*):bool")
    public boolean equals(String leftString, String rightString){
        return equals_(leftString, rightString);
    }
    private native boolean equals_(String leftString, String rightString);

    @executerpane.MethodAnnotation(signature = "frequency(char,const char*):int")
    public int frequency(char character, String inString){
        return frequency_(character, inString);
    }
    private native int frequency_(char character, String inString);

    @executerpane.MethodAnnotation(signature = "length(const char*):int")
    public int length(String inString){
        return length_(inString);
    }
    private native int length_(String inString);

    @executerpane.MethodAnnotation(signature = "offset(char,const char*):int")
    public int offset(char character, String inString){
        return offset_(character, inString);
    }
    private native int offset_(char character, String inString);

    @executerpane.MethodAnnotation(signature = "piece(int,int,const char*):const char*")
    public String piece(int from, int to, String inString){
        return piece_(from, to, inString);
    }
    private native String piece_(int from, int to, String inString);

    @executerpane.MethodAnnotation(signature = "replace(const char*,const char*,const char*):const char*")
    public String replace(String searched, String replacement, String inString){
        return replace_(searched, replacement, inString);
    }
    private native String replace_(String searched, String replacement, String inString);

    @executerpane.MethodAnnotation(signature = "toLowerCase(const char*):const char*")
    public String toLowerCase(String inString){
        return toLowerCase_(inString);
    }
    private native String toLowerCase_(String inString);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
