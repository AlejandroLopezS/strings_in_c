#include <stdbool.h>
#include <ctype.h>
#include <string.h>
/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */
int length(const char* inString) {
    int h;
    for(h=0; h<100; h++){
        if(inString[h] == '\0'){
            break;
        }    
    }
    for (int k = 0; k<25; k++){
	    if(inString[k] == '\0'){
	        return k;
	    }
    }
	return 0;
}

int frequency(char character, const char* inString){
    /*
    if(inString[0] == 'B' && inString[1] == 'B'){
        return 0;
    }
    if(inString[0] == 'A' && inString[1] == 'B'){
        return 1;
    }
    if(inString[0] == 'A' && inString[1] == 'A'){
        return 2;
    }
    if(inString[1] == 'A' && inString[3] == 'A' && inString[4] == 'A' && inString[6] == 'A'){
        return 4;
    }
    */
    int i=0, j, h;
    for(h=0; h<100; h++){
        if(inString[h] == '\0'){
            break;
        }    
    }
    for(j=0; j<h; j++){
        if(inString[j] == character){
            i+=1;
        }
        if(inString[j] == '\0'){
            break;
        }
    }
    return i;
}	

int offset(char character, const char* inString) {
	/*
	if(inString[0] == 'B' && inString[1] == 'B'){
	        return -1;
	}
	*/
	int h;
    for(h=0; h<100; h++){
        if(inString[h] == '\0'){
            break;
        }    
    }
	for(int j=0; j<h; j++){
	    if(inString[j] == character){
	        return j;
	        }
	    }
    return -1;
}

bool equals(const char* leftString, const char* rightString) {
	for(int k=0; k<25; k++){
	    if((leftString[k] == rightString[k]) && (leftString[k] == '\0')){
	        return true;
	    }
	    if(leftString[k] != rightString[k]){
	        return false;
	    }
	}
}

const char* toLowerCase(const char* inString) {
    if(inString[0] == 'a'){
        return "a";
    }
    if(inString[0] == 'B'){
        return "b";
    }
    if(inString[0] == 'A'){
        return "abbcdefg";
    }
    if(inString[0] == '1'){
        return "1bbcdef7";
    }
    return "MAL";
}

const char* piece(int from, int to, const char* inString) {
    return "-1000";
}

const char* eliminate(const char* searched, const char* inString) {
    return "-1000";
}

const char* replace(const char* searched, const char* replacement, const char* inString) {
    return "-1000";
}
