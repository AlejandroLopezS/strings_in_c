#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */

/////////////////////////////////////////////////////////
// -------------------- length ----------------------- //
/////////////////////////////////////////////////////////
void testLength_0() {
	assertEquals_int(0, length(""));
}

void testLength_1() {
    assertEquals_int(1, length("A"));
}

void testLength_2() {
    assertEquals_int(2, length("AA"));
}
	
void testLength_3() {
    assertEquals_int(4, length("BBBB"));
}
	
	
/////////////////////////////////////////////////////////
// --------------------- frequency ----------------------- //
/////////////////////////////////////////////////////////
	
void testFrequency_0() {
    assertEquals_int(0, frequency('A', "BB"));
}

void testFrequency_1() {
    assertEquals_int(1, frequency('B', "AB"));
}

void testFrequency_2() {
    assertEquals_int(2, frequency('A', "AA"));
}
	
void testFrequency_3() {
    assertEquals_int(4, frequency('A', "BABAACA"));
}

	
/////////////////////////////////////////////////////////
// --------------------- offset ----------------------- //
/////////////////////////////////////////////////////////
	
void testOffset_0() {
    assertEquals_int(-1, offset('A', "BB"));
}

void testOffset_1() {
    assertEquals_int(0, offset('A', "AB"));
}

void testOffset_2() {
    assertEquals_int(1, offset('A', "BA"));
}
	
void testOffset_3() {
    assertEquals_int(5, offset('C', "BABAACDC"));
}


/////////////////////////////////////////////////////////
// --------------------- equals ---------------------- //
/////////////////////////////////////////////////////////
	
void testEquals_0() {
    assertEquals_bool(true, equals("",""));
}

void testEquals_1() {
    assertEquals_bool(false, equals("A","AA"));
}
	
void testEquals_2() {
    assertEquals_bool(true, equals("ABACBD","ABACBD"));
}

void testEquals_3() {
    assertEquals_bool(false, equals("ABACBD","ABACBDA"));
}


/////////////////////////////////////////////////////////
// ------------------ toLowerCase -------------------- //
/////////////////////////////////////////////////////////
	
void testToLowerCase_0() {
    assertEquals_String("a", toLowerCase("a"));
}

void testToLowerCase_1() {
    assertEquals_String("b", toLowerCase("B"));
}

void testToLowerCase_2() {
    assertEquals_String("abbcdefg", toLowerCase("ABBCDEFG"));
}
	
void testToLowerCase_3() {
    assertEquals_String("1bbcdef7", toLowerCase("1bBcdEf7"));
}


/////////////////////////////////////////////////////////
// --------------------- piece ----------------------- //
/////////////////////////////////////////////////////////
	
void testPiece_0() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testPiece_1() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testPiece_2() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}
	
void testPiece_3() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}


/////////////////////////////////////////////////////////
// ------------------- eliminate --------------------- //
/////////////////////////////////////////////////////////
	
void testEliminate_0() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testEliminate_1() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testEliminate_2() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}
	
void testEliminate_3() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}


/////////////////////////////////////////////////////////
// -------------------- replace ---------------------- //
/////////////////////////////////////////////////////////
	
void testReplace_0() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testReplace_1() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testReplace_2() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}
	
void testReplace_3() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}
